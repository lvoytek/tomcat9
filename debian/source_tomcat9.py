#!/usr/bin/python3

'''apport package hook for tomcat9

(c) 2022 Canonical Ltd.
Author: Lena Voytek <lena.voytek@canonical.com>
'''

from apport.hookutils import *

def add_info(report):
    attach_file_if_exists(report, '/var/log/tomcat9/catalina.out')
